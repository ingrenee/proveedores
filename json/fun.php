<?php

function _helper_link($num = '12345678', $link = '') {

	return "<a href='" . $link . "'>" . $num . '</a>';

}

function _help_icono2($icono = 'search', $return = true) {

	$m = '<span class="fa fa-' . $icono . '" aria-hidden="true"></span>';

	if ($return):

		return $m;

	else:

		echo $m;

	endif;

}

function _help_icono($icono = 'search', $return = true) {

	$m = '<span class="glyphicon glyphicon-' . $icono . '" aria-hidden="true"></span>';

	if ($return):

		return $m;

	else:

		echo $m;

	endif;

}
function _helper_link_icon($icon = 'search', $class = 'btn btn-primary') {

	return "<a href='#' class='" . $class . "'>" . _help_icono($icon) . '</a>';

}

function _helper_link_icon2($icon = 'search', $class = 'btn btn-primary') {

	return "<a href='#' class='" . $class . "'>" . _help_icono2($icon) . '</a>';

}

function _helper_link_icon22($icon = 'search', $class = 'btn btn-primary') {

	return '<a href="#"" class="' . $class . ' ">' . _help_icono2($icon) . '</a>';

}