<?php

include 'fun.php';

function data() {
	$s = '
		check:<input type="checkbox">,
		id:' . _helper_link(rand(999, 999999), 'orden-compra-detalle-hoja-entrada.html') . ',
		numero:' . rand(9999, 1000) . ',
		fecha:12.12.2012,
		posicion:10000245,
		cantidad:1,
		unidad:AU,
		subtotal:6000.00,
		igv:600,
		total:70000.00,
		moneda:DolarUSA,
		documentos:' . _helper_link_icon2('file-text fx2', 'btn btn-primary btn-sm') . '
	';

	$partes = explode(',', $s);

	$fila = array();
	foreach ($partes as $k => $v) {

		$temp = explode(':', $v);

		$fila[trim($temp[0])] = $temp[1];
	}

	return $fila;
}
/*$fila['id'] = 1;
$fila['delegado'] = "Maria Fernandez";

$fila['desde'] = '01/01/2016';

$fila['hasta'] = '05/01/2016';

$fila['estado'] = 'Asignado';
$fila['comentarios'] = 'Comentarios extras';
$fila['accion'] = '<a href="#" class="btn btn-block btn-xs btn-danger"> Revocar </a>';
 */
/*

{"id":"#",
delegado:"Delegado a",
desde:"Desde",
hasta:"Hasta",
estado:"Estado",
comentarios:"Comentarios",
accion:""}

 */

$data[] = data();
$data[] = data();
$data[] = data();
$data[] = data();

$r['total'] = 10;
$r['rows'] = $data;

file_put_contents(basename(__FILE__, '.php') . '.json', json_encode($r));
echo json_encode($r);