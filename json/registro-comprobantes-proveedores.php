<?php

include 'fun.php';

function data() {
	$moneda[0] = 'Soles';
	$moneda[1] = 'Dolares';
	$estado[0] = 'Pendiente';
	$estado[1] = 'Pagado';
	$estado[2] = 'Preliminar';
	$s = '"id":' . _helper_link(rand(999, 999999), '#') . ',
"clase":FACTURA,
"nombre_proveedor":ID+Solutions,
"fecha_doc":08-08-2016,
"fecha_re":08-08-2016,
"fecha_es":09-09-2016,
"moneda":' . $moneda[rand(0, 1)] . ',
"importe":5#200.00,
"igv":936.00,
"importe_2":0.00,
"importe_3":6#200.30,
"datos":NAC-1234,
"estado_2":' . $estado[rand(0, 2)] . ',
"id2":' . _helper_link_icon22('file-text fx2', 'btn btn-primary btn-sm') . ',
"id3":' . _helper_link_icon22('file-text fx2', 'btn btn-primary btn-sm') . '';
	$partes = explode(',', $s);

	$fila = array();
	foreach ($partes as $k => $v) {

		$v = str_replace('#', ',', $v);
		$temp = explode(':', $v);

		$temp[0] = str_replace('"', '', $temp[0]);
		$fila[trim($temp[0])] = $temp[1];
	}

	return $fila;
}
/*$fila['id'] = 1;
$fila['delegado'] = "Maria Fernandez";

$fila['desde'] = '01/01/2016';

$fila['hasta'] = '05/01/2016';

$fila['estado'] = 'Asignado';
$fila['comentarios'] = 'Comentarios extras';
$fila['accion'] = '<a href="#" class="btn btn-block btn-xs btn-danger"> Revocar </a>';
 */
/*

{"id":"#",
delegado:"Delegado a",
desde:"Desde",
hasta:"Hasta",
estado:"Estado",
comentarios:"Comentarios",
accion:""}

 */

$data[] = data();
$data[] = data();
$data[] = data();
$data[] = data();

$r['total'] = 10;
$r['rows'] = $data;

file_put_contents(basename(__FILE__, '.php') . '.json', json_encode($r));
echo json_encode($r);