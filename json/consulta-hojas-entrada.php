<?php

include 'fun.php';

$s = '	id:' . _helper_link() . ',
		tipo:SERVICIOS,
		fecha:2016-05-05,
		subtotal:26000.00,
		igv:10000.00,
		total:36000.00,
		moneda: Dolar Usa,
		documentos:' . _helper_link_icon2('file-text fx2', 'btn btn-primary btn-sm') . '
	';

$s = 'id:123451285,
numero:79451245,
tipo:SERVICIOS;BIENES,
fecha:2016-05-05,
posicion:10;20;50,
fecha2:2016-02-02,
proveedor:4009005,
valor:52000,
igv:6000,
total:1000,
moneda:Dolar USA,
documentos:' . _helper_link_icon2('file-text fx2', 'btn btn-primary btn-sm');

$partes = explode(',', $s);

function _fila($partes) {
	$fila = array();
	foreach ($partes as $k => $v) {

		$temp = explode(':', $v);
		$temp2 = explode(';', $temp[1]);
		if (count($temp2) > 0):
			$temp[1] = $temp2[rand(0, count($temp2) - 1)];
		endif;

		$fila[trim($temp[0])] = $temp[1];
	}

	return $fila;
}

/*$fila['id'] = 1;
$fila['delegado'] = "Maria Fernandez";

$fila['desde'] = '01/01/2016';

$fila['hasta'] = '05/01/2016';

$fila['estado'] = 'Asignado';
$fila['comentarios'] = 'Comentarios extras';
$fila['accion'] = '<a href="#" class="btn btn-block btn-xs btn-danger"> Revocar </a>';
 */
/*

{"id":"#",
delegado:"Delegado a",
desde:"Desde",
hasta:"Hasta",
estado:"Estado",
comentarios:"Comentarios",
accion:""}

 */

$data[] = _fila($partes);
$data[] = _fila($partes);
$data[] = _fila($partes);
$data[] = _fila($partes);

$r['total'] = 10;
$r['rows'] = $data;
file_put_contents('consulta-hojas-entrada.json', json_encode($r));
echo json_encode($r);